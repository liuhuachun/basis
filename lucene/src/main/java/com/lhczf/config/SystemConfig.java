package com.lhczf.config;

import com.lhczf.util.YamlUtil;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Getter
@Setter
public class SystemConfig {
    private static final Logger logger = LoggerFactory.getLogger(SystemConfig.class);
    private static final String THE_ROOT_DIR_OF_INDEX = "system.index.root";
    private static final String THE_DIR_OF_DATA = "system.data.root";

    public String theRootDirOfIndex = "index";
    public String theDirOfIndexData = "data";

    public void loadSystemConfig() {
        Map<String, String> configMap = YamlUtil.loadConfig();
        String rootDir = configMap.get(THE_ROOT_DIR_OF_INDEX);
        if (rootDir != null && !rootDir.isEmpty()) {
            theRootDirOfIndex = rootDir;
        }
        rootDir = configMap.get(THE_DIR_OF_DATA);
        if (rootDir != null && !rootDir.isEmpty()) {
            theDirOfIndexData = rootDir;
        }
    }
}
