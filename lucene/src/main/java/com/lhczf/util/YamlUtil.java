package com.lhczf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class YamlUtil {

    private static final Logger logger = LoggerFactory.getLogger(YamlUtil.class);
    private static Yaml yaml = new Yaml();
    private static final String THE_CONFIG_OF_SYSTEM = "config/system-config.yml" ;

    public static Map loadConfig() {
        Map<String, String> ymlConfig;
        InputStream input = YamlUtil.class.getClassLoader().getResourceAsStream(THE_CONFIG_OF_SYSTEM);
        ymlConfig = yaml.load(input);
        if (ymlConfig == null ) {
            logger.error("加载系统配置文件出错，没有加载到系统配置。");
            ymlConfig = new HashMap<>(16);
        }
        return ymlConfig;
    }
}
