package com.lhczf.net;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface NIOConnection {
    void register() throws IOException;
    void handle(byte[] data);
    void write(ByteBuffer buffer);
}
