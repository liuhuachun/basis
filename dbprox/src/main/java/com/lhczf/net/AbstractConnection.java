package com.lhczf.net;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.NetworkChannel;
import java.util.concurrent.ConcurrentLinkedQueue;

public class AbstractConnection implements NIOConnection {

    protected final ConcurrentLinkedQueue<ByteBuffer> writeQueue = new ConcurrentLinkedQueue<>();
    private SocketWR socketWR;
    protected final NetworkChannel channel;

    public AbstractConnection(NetworkChannel channel) {
        this.channel = channel;
        socketWR = new AIOSocketWR();
    }

    @Override
    public void register() throws IOException {

    }

    @Override
    public void handle(byte[] data) {

    }

    @Override
    public void write(ByteBuffer buffer) {
        writeQueue.offer(buffer);
        //TODO 发送数据
    }
}
