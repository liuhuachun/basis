package com.lhczf.net;

public interface SocketAcceptor {
    void start();

    String getName();

    int getPort();
}
