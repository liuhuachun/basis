package com.lhczf.proxy;

import com.lhczf.net.AIOServerHandler;
import com.lhczf.proxy.buffer.BufferPool;

import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Collection;

public interface SessionManager<T extends Session> {
    public T createSession(Object keyAttachement, BufferPool bufPool, Selector nioSelector, SocketChannel channel,
                           boolean isAcceptedCon) throws IOException;

    public Collection<T> getAllSessions();

    public AIOServerHandler getDefaultSessionHandler();

    public void removeSession(Session session);
}
