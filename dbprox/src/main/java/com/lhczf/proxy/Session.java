package com.lhczf.proxy;

import java.nio.channels.SocketChannel;

public interface Session {
    public SocketChannel channel();
    boolean isClosed();
    public <T extends Session> SessionManager<T> getSessionManager();
}
