package com.lhczf.proxy;

import com.lhczf.config.ProxyYmlConfig;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class ProxyRuntime {

    public static final ProxyRuntime INSTANCE = new ProxyRuntime();
    private static final Logger logger = LoggerFactory.getLogger(ProxyRuntime.class);

    private ProxyYmlConfig proxyConfig = new ProxyYmlConfig();
}
