package com.lhczf.config;

import lombok.Data;

@Data
public class ProxyBean {
    private String ip = "0.0.0.0";
    private int port = 6666;
    private boolean isAio = false;
}
