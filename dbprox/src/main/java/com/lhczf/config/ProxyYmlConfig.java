package com.lhczf.config;

import lombok.Data;

@Data
public class ProxyYmlConfig implements Configurable {
    private ProxyBean proxy = new ProxyBean();
}
