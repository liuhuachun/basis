package com.lhczf;

import com.lhczf.config.ProxyBean;
import com.lhczf.config.ProxyYmlConfig;
import com.lhczf.proxy.ProxyRuntime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxyStarter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyStarter.class);
    public static final ProxyStarter INSTANCE = new ProxyStarter();

    public void start() {
        ProxyRuntime runtime = ProxyRuntime.INSTANCE;
        ProxyYmlConfig config = runtime.getProxyConfig();
        ProxyBean proxybean = config.getProxy();


    }
}
