package com.lhczf;

import com.lhczf.config.ProxyBean;
import com.lhczf.config.ProxyYmlConfig;
import com.lhczf.net.AIOBind;
import com.lhczf.net.NIOBind;
import com.lhczf.proxy.ProxyRuntime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Hello world!
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        ProxyRuntime runtime = ProxyRuntime.INSTANCE;
        ProxyYmlConfig config = runtime.getProxyConfig();
        ProxyBean proxybean = config.getProxy();

        boolean isAio = proxybean.isAio();
        LOGGER.info("the nio or aio is used ? :" + (isAio ? "aio" : "nio"));
        if (isAio) {
            AIOBind.beginToBindAddr(proxybean.getIp(), proxybean.getPort());
        } else {
            try {
                NIOBind.beginToBindAddr(proxybean.getIp(), proxybean.getPort());
            } catch (IOException e) {
                LOGGER.error("", e);
            }
        }
    }
}
