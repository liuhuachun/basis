package com.lhczf;

import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AppTest {
    @Test
    public void shouldAnswerWithTrue() {
        SocketChannel client = null;
        try {
            client = SocketChannel.open();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            client.connect(new InetSocketAddress("127.0.0.1", 6666));
            ByteBuffer writeBuffer = ByteBuffer.allocate(32);
            ByteBuffer readBuffer = ByteBuffer.allocate(32);

            writeBuffer.put("hello".getBytes());
            writeBuffer.flip();
            while (true) {
                writeBuffer.rewind(); // sets the position back to 0
                client.write(writeBuffer); // hello
                readBuffer.clear(); // make buffer ready for writing
                client.read(readBuffer); // recieved
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
