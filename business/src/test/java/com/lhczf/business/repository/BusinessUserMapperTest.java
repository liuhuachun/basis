package com.lhczf.business.repository;

import com.lhczf.business.domain.BusinessUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessUserMapperTest {

    @Resource
    private BusinessUserMapper businessUserMapper;

    @Test
    public void addUser() {
        var user = new BusinessUser();
        user.setGmtCreate(new Date());
        user.setGmtModified(new Date());
        var id = businessUserMapper.insert(user);
        Assert.assertNotEquals(0, id);
    }
}