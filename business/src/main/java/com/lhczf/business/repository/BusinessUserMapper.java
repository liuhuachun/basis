package com.lhczf.business.repository;

import com.lhczf.business.domain.BusinessUser;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface BusinessUserMapper extends Mapper<BusinessUser> {

}