package com.lhczf.business.domain;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "business_user")
public class BusinessUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`gmt_create`")
    private Date gmtCreate;

    @Column(name = "`gmt_modified`")
    private Date gmtModified;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "login_name")
    private String loginName;

    private String password;

    private Integer age;

    private String sex;

    private String email;

    private String address;

    private String telephone;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "is_locked")
    private Byte isLocked;
}