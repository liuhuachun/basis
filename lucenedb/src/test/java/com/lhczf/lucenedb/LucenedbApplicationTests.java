package com.lhczf.lucenedb;

import com.lhczf.lucenedb.service.LuceneDbServer;
import com.lhczf.lucenedb.util.SpringUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LucenedbApplicationTests {

    @Before
    public void setUp() throws Exception {
        LuceneDbServer server = SpringUtil.getBean(LuceneDbServer.class);
        server.init();
    }

    @Test
    public void contextLoads() {}
}
