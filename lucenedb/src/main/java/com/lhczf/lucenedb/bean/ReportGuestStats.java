package com.lhczf.lucenedb.bean;

import lombok.Data;

@Data
public class ReportGuestStats {
    private Long id;

    private Long statsTime;

    private String protectObjectName;

    private String destIp;

    private Integer dbPort;

    private Integer dbType;

    private Integer srcIpNum;

    private Integer dbUserNum;

    private Long auditTotal;

}
