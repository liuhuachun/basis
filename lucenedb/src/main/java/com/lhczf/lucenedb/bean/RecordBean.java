package com.lhczf.lucenedb.bean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RecordBean implements Serializable {

    private long id;
    private long happenTime;
    private String srcIp;
    private Integer srcPort;
    private String srcMac;
    private String systemUser;
    private String systemHost;
    private String destIp;
    private Integer destPort;
    private String destMac;

    private String visitTool;
    private String appAccount;
    private String sessionId;
    private Integer dbType;
    private String dbUser;
    private String operType;
    private String dbName;
    private String tableName;
//    private Integer tableNum;
    private String fieldName;

    private String operSentence;
    private Integer operSentenceLen;
    private String sqlBindValue;
    private Integer rowNum;
    private double sqlExecTime;
    private Integer sqlResponse;
    private String returnContent;
    private Integer returnContentLen;
    private Integer dealState;
    private String protectObjectName;

    private Integer ruleType;  //  StringField 规则名称
    private String ruleName;
    private Integer riskLev;


}
