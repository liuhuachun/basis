package com.lhczf.lucenedb.bean;

import lombok.Data;

@Data
public class ReportMainStats {
    private Long id;

    private Long statsTime;

    private String protectObjectName;

    private String visitTool;

    private String srcIp;

    private String dbUser;

    private String operType;

    private Long operNum;
}
