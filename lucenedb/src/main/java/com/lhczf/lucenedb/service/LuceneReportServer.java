package com.lhczf.lucenedb.service;

import com.lhczf.lucenedb.threads.MergeReportThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LuceneReportServer {

    public void startMergeReportThread(String threadName) {
        MergeReportThread mergeReportThread = new MergeReportThread(threadName);
        mergeReportThread.start();
    }
}
