package com.lhczf.lucenedb.service;


import com.lhczf.lucenedb.util.LuceneUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Slf4j
@Service
public class BusinessServer {
    @Resource
    private LuceneUtil luceneUtil;

    public int getTotal() {
        IndexSearcher indexSearcher = luceneUtil.makeCommSearcher();
        Query query = IntPoint.newRangeQuery("riskLev", 0, 5);
        int total = 0;
        try {
            total = indexSearcher.count(query);
        } catch (IOException e) {
            log.error("查询过程中出现异常，查询内容为：{}", query.toString());
        }
        return total;
    }
}
