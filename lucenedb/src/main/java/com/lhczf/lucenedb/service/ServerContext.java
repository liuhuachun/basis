package com.lhczf.lucenedb.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.IndexWriter;
import org.springframework.stereotype.Service;

@Service
public class ServerContext {

    @Getter
    @Setter
    public int threadTotal;

    @Getter
    @Setter
    private DirectoryTaxonomyWriter taxoWriter;

    @Getter
    @Setter
    private IndexWriter integrWriter;
}
