package com.lhczf.lucenedb.service.impl;


import com.lhczf.lucenedb.bean.City;
import com.lhczf.lucenedb.dao.CityDao;
import com.lhczf.lucenedb.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 城市业务逻辑实现类
 * <p>
 * Created by bysocket on 07/02/2017.
 */
@Slf4j
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    /**
     * 获取城市逻辑：
     * 如果缓存存在，从缓存中获取城市信息
     * 如果缓存不存在，从 DB 中获取城市信息，然后插入缓存
     */
    public City findCityById(Long id) {
        // 从 DB 中获取城市信息
        City city = cityDao.findById(id);
        log.info("CityServiceImpl.findCityById() : >> " + city.toString());
        return city;
    }

    @Override
    public Long saveCity(City city) {
        log.info("CityServiceImpl.saveCity() : >> " + city.toString());
        return cityDao.saveCity(city);
    }

    /**
     * 更新城市逻辑：
     * 如果缓存存在，删除
     * 如果缓存不存在，不操作
     */
    @Override
    public Long updateCity(City city) {
        log.info("CityServiceImpl.updateCity() : >> " + city.toString());
        Long ret = cityDao.updateCity(city);
        return ret;
    }

    @Override
    public Long deleteCity(Long id) {
        Long ret = cityDao.deleteCity(id);
        log.info("CityServiceImpl.deleteCity() : >> " + ret.toString());
        return ret;
    }
}
