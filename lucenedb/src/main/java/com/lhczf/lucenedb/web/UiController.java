package com.lhczf.lucenedb.web;

import com.lhczf.lucenedb.service.BusinessServer;
import com.lhczf.lucenedb.util.SpringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;

@Controller
public class UiController {

    @GetMapping
    public ModelAndView index(Model model) {
        model.addAttribute("name", "carl");
        BusinessServer server = SpringUtil.getBean(BusinessServer.class);
        LocalDateTime start = LocalDateTime.now();
        int total = server.getTotal();
        LocalDateTime end = LocalDateTime.now();
        Duration duration = Duration.between(start, end);

        model.addAttribute("total", total);
        model.addAttribute("time", duration.toMillis());
        String dataDir = SpringUtil.getProperValue("system.lucenedb.data.root");
        File file = new File(dataDir);
        model.addAttribute("data", file.getAbsolutePath());
        dataDir = SpringUtil.getProperValue("system.lucenedb.index.root");
        file = new File(dataDir);
        model.addAttribute("index", file.getAbsolutePath());

        dataDir = SpringUtil.getProperValue("system.lucenedb.index.backup");
        file = new File(dataDir);
        model.addAttribute("backup", file.getAbsolutePath());

        return new ModelAndView("index", "system", model);
    }
}
