package com.lhczf.lucenedb.extend.collector;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.SimpleCollector;

import java.io.IOException;

@Slf4j
public class ResultSetCollector extends SimpleCollector {

    @Override
    public void collect(int doc) throws IOException {
        log.info(String.valueOf(doc));
    }

    @Override
    public boolean needsScores() {
        return false;
    }
}
