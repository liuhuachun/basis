package com.lhczf.lucenedb.extend.scorer;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;

public class EmptySimilarity extends Similarity {

    @Override
    public long computeNorm(FieldInvertState state) {
        return 0;
    }

    @Override
    public SimWeight computeWeight(float boost, CollectionStatistics collectionStats, TermStatistics... termStats) {
        return new EmptySimWeight();
    }

    @Override
    public SimScorer simScorer(SimWeight weight, LeafReaderContext context) throws IOException {
        return new EmptyScorer();
    }

    public static class EmptyScorer extends SimScorer {

        @Override
        public float score(int doc, float freq) throws IOException {
            return 0;
        }

        @Override
        public float computeSlopFactor(int distance) {
            return 0;
        }

        @Override
        public float computePayloadFactor(int doc, int start, int end, BytesRef payload) {
            return 0;
        }
    }

    public static class EmptySimWeight extends SimWeight {
    }

}
