package com.lhczf.lucenedb.dao;

import com.lhczf.lucenedb.bean.ReportGuestStats;

public interface ReportGuestStatsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ReportGuestStats record);

    int insertSelective(ReportGuestStats record);

    ReportGuestStats selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ReportGuestStats record);

    int updateByPrimaryKey(ReportGuestStats record);
}

