package com.lhczf.lucenedb.dao;

import com.lhczf.lucenedb.bean.ReportMainStats;

public interface ReportMainStatsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ReportMainStats record);

    int insertSelective(ReportMainStats record);

    ReportMainStats selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ReportMainStats record);

    int updateByPrimaryKey(ReportMainStats record);
}
