
-- 报表中间表
CREATE TABLE `report_table_info` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`table_name`  varchar(50) NOT NULL,
`table_field` varchar(255) NOT NULL,
`stats_time_point`  bigint(8) NOT NULL,
PRIMARY KEY (`id`),
UNIQUE INDEX `table_name` (`table_name`) USING BTREE
)ENGINE=InnoDB DEFAULT CHARACTER SET=utf8mb4;

-- 客体信息
CREATE TABLE `report_guest_stats` (
  `id` bigint(8) unsigned NOT NULL AUTO_INCREMENT,
  `stats_time` bigint(8) NOT NULL,
  `protect_object_name` varchar(50) DEFAULT NULL,
  `dest_ip` varchar(50) DEFAULT NULL,
  `dest_port` int(4) DEFAULT NULL,
  `db_type` int(4) DEFAULT NULL,
  `src_ip_num` int(4) DEFAULT NULL,
  `db_user_num` int(4) DEFAULT NULL,
  `audit_total` bigint(8) NOT NULL,
  PRIMARY KEY (`id`,`stats_time`),
  KEY `stats_time` (`stats_time`) USING BTREE,
  KEY `protect_object_name` (`protect_object_name`) USING BTREE,
  KEY `dest_ip` (`dest_ip`) USING BTREE,
  KEY `dest_port` (`dest_port`) USING BTREE,
  KEY `db_type` (`db_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET= utf8mb4
PARTITION BY RANGE COLUMNS (`stats_time`)
(PARTITION p20190120 VALUES LESS THAN(1547913600 ) ENGINE = InnoDB);

-- 主体信息
CREATE TABLE `report_main_stats` (
  `id` bigint(8) unsigned NOT NULL AUTO_INCREMENT,
  `stats_time` bigint(8) NOT NULL,
  `protect_object_name` varchar(50) DEFAULT NULL,
  `visit_tool` varchar(50) DEFAULT NULL,
  `src_ip` varchar(50) DEFAULT NULL,
  `db_user` varchar(50) DEFAULT NULL,
  `oper_type` varchar(30) DEFAULT NULL,
  `oper_num` bigint(8) NOT NULL,
  PRIMARY KEY (`id`,`stats_time`),
  KEY `stats_time` (`stats_time`) USING BTREE,
  KEY `protect_object_name` (`protect_object_name`) USING BTREE,
  KEY `visit_tool` (`visit_tool`) USING BTREE,
  KEY `src_ip` (`src_ip`) USING BTREE,
  KEY `db_user` (`db_user`) USING BTREE,
  KEY `oper_type` (`oper_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET= utf8mb4
PARTITION BY RANGE COLUMNS (`stats_time`)
(PARTITION p20190120 VALUES LESS THAN(1547913600) ENGINE = InnoDB);

-- 测试表
CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;